core = 7.x
api  = 2

; WYSIKit Makefile

projects[entity][version] = 1.5
projects[entity][subdir] = contrib

projects[image_resize_filter][version] = 1.14
projects[image_resize_filter][subdir] = contrib

projects[linkit][version] = 3.3
projects[linkit][subdir] = contrib

projects[pathologic][version] = 2.12
projects[pathologic][subdir] = contrib

projects[transliteration][version] = 3.1
projects[transliteration][subdir] = contrib

projects[wysiwyg][version] = 2.x-dev
projects[wysiwyg][subdir] = contrib
projects[wysiwyg][download][type] = git
projects[wysiwyg][download][revision] = 40b26a1
projects[wysiwyg][download][branch] = 7.x-2.x
;projects[wysiwyg][patch][1853550] = https://drupal.org/files/wysiwyg-ckeditor-4.1853550.136.patch

projects[wysiwyg_filter][version] = 1.6-rc2
projects[wysiwyg_filter][subdir] = contrib

; Libraries

libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.6/ckeditor_4.4.6_full.zip

libraries[markitup][download][type] = get
libraries[markitup][download][url] = https://github.com/markitup/1.x/tarball/dcce656e9325a88e7e56e1cb465623e5b8ee82ae
libraries[markitup][patch][1715642] = http://drupal.org/files/1715642-adding-html-set-markitup-editor.patch

