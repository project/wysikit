/**
 * @file
 * Custom configuration for ckeditor.
 */

CKEDITOR.editorConfig = function( config )
{
  // config.styleSet is an array of objects that define each style available
  // in the font styles tool in the ckeditor toolbar
  config.stylesSet =
  [
        /* Block Styles */
        { name : 'Paragraph'   , element : 'p' },
        { name : 'Heading 2'   , element : 'h2' },
        { name : 'Heading 3'   , element : 'h3' },
        { name : 'Heading 4'   , element : 'h4' },
        { name : 'Heading 5'   , element : 'h5' },
        { name: 'Telephone', element: 'span', attributes: { 'class': 'tel' } },
        { name : 'Preformatted Text', element : 'pre' },

            // Object Styles
        {
            name: 'Image on Left',
            element: 'img',
            attributes : { 'class' : 'left' }
        },
        {
            name: 'Image on Right',
            element: 'img',
            attributes : { 'class' : 'right' }
        }
  ];
};
