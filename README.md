## Purpose Wysikit module?

Wysikit module's goal is to simplify the process of setting up a WYSIWYG editor
for Drupal 7 sites. Setting up a WYSIWYG editor can be a tedious and error prone
process. Ideally, downloading and installing Wysikit on your Drupal site will
give you a full featured WYSIWIG editor that just works™.

### Features

* CKEditor 4.3+ with a simple configuration of commonly requested features
* Configuration of the text formats and WYSIWYG editor happens during
installation which allows you to create a feature module to override the
configuration in a maintainable way.
* In-line images (Wysikit Media) using the [Media module](http://drupal.org/project/media).
* Linkit configuration which allows you to link to internal and external content.
* Tables

### Overriding the default configuration.

Wysikit will set up text formats and configuration on install. For many sites, the out
of the box configuration will work just fine. For those of you that need to
disable/enable CKeditor buttons the recommended way of doing so is to create a
site specific feature module.

#### Example

Name of your feature module: mysite_wysiwyg

Export the following items:

* Text formats 'wysikit_html_text' and 'wysikit_wysiwyg_text'
* WYSIWYG configuration 'wysikit_html_text' and 'wysikit_wysiwyg_text'
* Any additional configuration that makes sense.

## Dependencies

**Modules**

* ctools
* features
* filter
* image_resize_filter
* linkit 3.x
* pathologic
* strongarm
* wysiwyg
* wysiwyg_filter

**Libraries**

* CKEditor 4.3 FULL VERSION http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.3.2/ckeditor_4.3.2_full.zip
* Markitup https://github.com/markitup/1.x/tarball/dcce656e9325a88e7e56e1cb465623e5b8ee82ae
* Patch to Markitup http://drupal.org/files/1715642-adding-html-set-markitup-editor.patch

## Installation

* Download and install all dependent modules.
* Enable Wysikit and Wysikit Media(If you want to embed images).
* Enable the wysikit_html_text and wysikit_wysiwyg_text text formats.
* You may need to adjust the order of the text formats (Administration »
Configuration » Content authoring) so that the new text formats will be the
default text formats for your text formats.


### Drush installation

Wysikit includes a Drush make file. If you build your site with Drush make then
Drush will find this file and download the required modules.

$ drush dl wysikit
$ drush make sites/all/modules/contrib/wysikit/wysikit.make --no-core

## Acknowledgments

A large portion of this module was originally from the [Panopoly WYSIWYG](https://www.drupal.org/project/panopoly_wysiwyg) module.
There are two key differences between Wysikit and Panopoly WYSIWYG. The first is
 the editor choice. Panopoly WYSIWYG uses TinyMCE while Wysikit is using
CKEditor(the editor in Drupal 8 core). Panopoly WYSIWYG was created to provide
configuration for Panopoly; Wysikit was designed to be used on any Drupal 7 site.
In addition to this Wysikit was designed to be easy to featurize per site.
